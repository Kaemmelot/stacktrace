<?php

namespace Kaemmelot\StackTrace;

use Kaemmelot\StackTrace\FilterCriteria\FilterCriterion;

class Filter
{
    const AFTER_FIRST  = "AFTER_FIRST";
    const AFTER_LAST   = "AFTER_LAST";
    const BEFORE_FIRST = "BEFORE_FIRST";
    const BEFORE_LAST  = "BEFORE_LAST";
    const SINGLE       = "SINGLE";
    const GROUP        = "GROUP";

    /**
     * @var array<string,array<string,mixed>>
     */
    private $exclude;

    /**
     * @param Filter|null $clone Filter to clone.
     */
    public function __construct(Filter $clone = null)
    {
        $this->exclude = ($clone !== null) ? $clone->exclude : array();
    }

    /**
     * @param array $array
     * @param  int  $first
     * @param   int $last
     */
    private function unsetKeys(array &$array, $first, $last)
    {
        for ($i = $first; $i <= $last; $i++)
            unset($array[$i]);
    }

    /**
     * @param StackTrace $trace
     * @return StackTrace
     */
    public function filter(StackTrace $trace)
    {
        $frames = $trace->getCallFrames();
        $last = \count($frames) - 1;
        $criteria = \array_map(function ($entry) { return $entry["criterion"]; }, $this->exclude);
        $matches = FilterCriterion::checkCriteriaOnFrames($criteria, $frames);
        foreach ($matches as $criterionName => $match)
        {
            $inclusiveOffset = $this->exclude[$criterionName]["inclusive"] ? 0 : 1;
            switch ($this->exclude[$criterionName]["type"]) // TODO check if it is correct now
            {
                case self::BEFORE_LAST:
                    $this->unsetKeys($frames, \min($match) + $inclusiveOffset, $last);
                    break;
                case self::BEFORE_FIRST:
                    $this->unsetKeys($frames, \max($match) + $inclusiveOffset, $last);
                    break;
                case self::AFTER_LAST:
                    $this->unsetKeys($frames, 0, \min($match) - $inclusiveOffset);
                    break;
                case self::AFTER_FIRST:
                    $this->unsetKeys($frames, 0, \max($match) - $inclusiveOffset);
                    break;
                case self::GROUP:
                    $this->unsetKeys($frames, \min($match) + $inclusiveOffset, \max($match) - $inclusiveOffset);
                    break;
                case self::SINGLE:
                default:
                    foreach ($match as $frameKey)
                        $this->unsetKeys($frames, $frameKey, $frameKey);
                    break;
            }
        }

        return new StackTrace(\array_values($frames));
    }

    /**
     * @param FilterCriterion $criterion
     * @param string          $type
     * @param bool            $inclusive Ignored on $type==SINGLE
     * @return Filter
     */
    public function exclude(FilterCriterion $criterion, $type = SINGLE, $inclusive = false)
    {
        $this->exclude[(string) $criterion] =
            array("criterion" => $criterion, "type" => $type, "inclusive" => $inclusive);

        return $this;
    }
}
