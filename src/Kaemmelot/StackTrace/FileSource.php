<?php

namespace Kaemmelot\StackTrace;

class FileSource extends Source
{
    /**
     * @var string
     */
    private $path;

    /**
     * @param string $path
     */
    public function __construct($path)
    {
        $this->path = $path;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return \file_get_contents($this->path);
    }
}
