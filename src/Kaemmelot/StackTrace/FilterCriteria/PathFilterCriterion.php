<?php

namespace Kaemmelot\StackTrace\FilterCriteria;

use Kaemmelot\StackTrace\CallFrames\CallFrame;
use Kaemmelot\StackTrace\FileSource;

class PathFilterCriterion extends FilterCriterion
{
    /**
     * @var string
     */
    private $path;

    /**
     * @var bool
     */
    private $exact;

    /**
     * @param string $path
     * @return bool
     */
    private function meetsPath($path)
    {
        return $this->exact ?
            $this->path === $path :
            \substr_compare($this->path, $path, 0, \strlen($this->path)) === 0;
    }

    /**
     * @param string $path
     * @param bool   $exact
     */
    public function __construct($path, $exact = false)
    {
        $this->path = $path;
        $this->exact = $exact;
    }

    /**
     * @param CallFrame $callFrame
     * @return bool
     */
    public function meetsCallFrame(CallFrame $callFrame)
    {
        return $callFrame->hasSource() && ($callFrame->getSource() instanceof FileSource) &&
               $this->meetsPath($callFrame->getSource()->getPath());
    }

    /**
     * @return string
     */
    protected function getIdentifier()
    {
        return $this->path;
    }
}
