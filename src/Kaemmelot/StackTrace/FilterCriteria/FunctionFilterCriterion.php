<?php

namespace Kaemmelot\StackTrace\FilterCriteria;

use Kaemmelot\StackTrace\CallFrames\CallFrame;
use Kaemmelot\StackTrace\CallFrames\FunctionCallFrame;
use Kaemmelot\StackTrace\CallFrames\MethodCallFrame;

class FunctionFilterCriterion extends FilterCriterion
{
    /**
     * @var string
     */
    private $function;

    /**
     * @var bool
     */
    private $mayBeMethod;

    /**
     * @param string $function
     * @param bool   $mayBeMethod
     */
    public function __construct($function, $mayBeMethod = false)
    {
        $this->function = $function;
        $this->mayBeMethod = $mayBeMethod;
    }

    /**
     * @param CallFrame $callFrame
     * @return bool
     */
    public function meetsCallFrame(CallFrame $callFrame)
    {
        return (($callFrame instanceof FunctionCallFrame) && ($callFrame->getTargetFunctionName() === $this->function))
               || ($this->mayBeMethod && ($callFrame instanceof MethodCallFrame) &&
                   ($callFrame->getTargetMethodName() === $this->function));
    }

    /**
     * @return string
     */
    protected function getIdentifier()
    {
        return $this->function . ($this->mayBeMethod ? 1 : 0);
    }
}
