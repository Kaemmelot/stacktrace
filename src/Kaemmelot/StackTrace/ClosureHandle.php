<?php

namespace Kaemmelot\StackTrace;

/**
 * @see https://bugs.php.net/bug.php?id=62325 #62325: Debug Backtrace gives no
 * handle to anonymous functions
 */
class ClosureHandle
{
    /**
     * @var int
     */
    private $startLine;

    /**
     * @var int
     */
    private $endLine;

    /**
     * @var Source
     */
    private $source;

    /**
     * @param Source $source
     * @param int    $startLine
     * @param int    $endLine
     */
    public function __construct(Source $source, $startLine, $endLine)
    {
        $this->source = $source;
        $this->startLine = $startLine;
        $this->endLine = $endLine;
    }

    /**
     * @return Source
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @return int
     */
    public function getStartLine()
    {
        return $this->startLine;
    }

    /**
     * @return int
     */
    public function getEndLine()
    {
        return $this->endLine;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return \implode("\n", $this->getContentLines());
    }

    /**
     * @return string[]
     */
    public function getContentLines()
    {
        // TODO parse?
        return \array_slice($this->source->getContentLines(),
                            $this->startLine - 1,
                            $this->endLine - $this->startLine - 1); // line 1 == array[0] -> -1
    }
}
