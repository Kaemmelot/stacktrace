<?php

namespace Kaemmelot\StackTrace\CallFrames;

use Kaemmelot\StackTrace\Source;
use ReflectionClass;
use ReflectionMethod;

abstract class MethodCallFrame extends CallFrame
{
    /**
     * @var string
     */
    private $targetMethodName;

    /**
     * @var string
     */
    private $targetClass;

    /**
     * @var ReflectionClass|null
     */
    private $targetReflectionClass = null;

    /**
     * @var ReflectionMethod|null
     */
    private $targetReflectionMethod = null;

    /**
     * @param string      $class
     * @param string      $methodName
     * @param array       $args
     * @param int         $line
     * @param Source|null $source
     */
    public function __construct($class, $methodName, array $args, $line, Source $source = null)
    {
        parent::__construct($args, $line, $source);
        $this->targetClass = $class;
        $this->targetMethodName = $methodName;
    }

    /**
     * @return ReflectionClass
     */
    public function getTargetReflectionClass()
    {
        if ($this->targetReflectionClass === null)
            $this->targetReflectionClass = new ReflectionClass($this->targetClass);

        return $this->targetReflectionClass;
    }

    /**
     * @return ReflectionMethod
     */
    public function getTargetReflectionFunction()
    {
        if ($this->targetReflectionMethod === null)
            $this->targetReflectionMethod = new ReflectionMethod($this->targetClass,
                                                                 $this->targetMethodName);

        return $this->targetReflectionMethod;
    }

    /**
     * @return string
     */
    public function getTargetMethodName()
    {
        return $this->targetMethodName;
    }

    /**
     * @return string
     */
    public function getTargetClassName()
    {
        return $this->targetClass;
    }
}
