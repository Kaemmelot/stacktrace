<?php

namespace Kaemmelot\StackTrace\CallFrames;

use Exception;
use Kaemmelot\StackTrace\Source;
use ReflectionFunction;
use ReflectionFunctionAbstract;

class FunctionCallFrame extends CallFrame
{
    const CONTROL_STRUCTURE = "CONTROL_STRUCTURE";
    const CLOSURE           = "CLOSURE";
    const USER_FUNCTION     = "USER_FUNCTION";
    const PHP_FUNCTION      = "PHP_FUNCTION";

    /**
     * Pairs of functionName->parameterNames
     * @var array<string,string[]>
     */
    private static $controlStructures = array(
        "include"      => array("file" => null),
        "include_once" => array("file" => null),
        "require"      => array("file" => null),
        "require_once" => array("file" => null),
        "eval"         => array("code_str" => null)
    );

    /**
     * @var array|null
     */
    private static $phpFunctions = null;

    /**
     * @var string
     */
    private $targetFunctionName;

    /**
     * @var int|null
     */
    private $targetFunctionType = null;

    /**
     * @var ReflectionFunctionAbstract|null
     */
    private $reflectionFunction = null;

    /**
     * @param string $functionName
     * @return int|null
     */
    public static function getFunctionTypeForName($functionName)
    {
        if (isset(self::$controlStructures[$functionName]))
            return self::CONTROL_STRUCTURE;
        if (\strpos($functionName, "{closure}") !== false)
            return self::CLOSURE;
        if (!\function_exists($functionName))
            return null;

        if (self::$phpFunctions === null)
        {
            $funcs = \get_defined_functions();
            self::$phpFunctions = \array_flip($funcs["internal"]);
        }
        if (isset(self::$phpFunctions[$functionName]))
            return self::PHP_FUNCTION;
        else
            return self::USER_FUNCTION;
    }

    /**
     * @param string      $functionName
     * @param array       $args
     * @param int         $line
     * @param Source|null $source
     * @throws Exception If the function does not exist.
     */
    public function __construct($functionName, array $args = array(), $line = 0, Source $source = null)
    {
        parent::__construct($args, $line, $source);
        $this->targetFunctionName = $functionName;
        if (!\function_exists($functionName)
            && (\strpos($functionName, "{closure}") === false)
            && !isset(self::$controlStructures[$functionName])
        )
            throw new Exception("Not a function: $functionName");
    }

    /**
     * @return ReflectionFunctionAbstract|null
     */
    public function getTargetReflectionFunction()
    {
        if (\function_exists($this->targetFunctionName)
            && ($this->reflectionFunction === null)
        )
            $this->reflectionFunction = new ReflectionFunction($this->targetFunctionName);

        return $this->reflectionFunction;
    }

    /**
     * @return string
     */
    public function getTargetFunctionName()
    {
        return $this->targetFunctionName;
    }

    /**
     * @return int
     */
    public function getTargetFunctionType()
    {
        if ($this->targetFunctionType === null)
            $this->targetFunctionType = self::getFunctionTypeForName($this->targetFunctionName);

        return $this->targetFunctionType;
    }

    /**
     * @return bool
     */
    public function isTargetUserFunction()
    {
        return $this->getTargetFunctionType() === self::USER_FUNCTION;
    }

    /**
     * @return bool
     */
    public function isTargetPhpFunction()
    {
        return $this->getTargetFunctionType() === self::PHP_FUNCTION;
    }

    /**
     * @return bool
     */
    public function isTargetControlStructure()
    {
        return isset(self::$controlStructures[$this->targetFunctionName]);
    }

    /**
     * @return bool
     */
    public function isTargetClosure()
    {
        return \strpos($this->targetFunctionName, "{closure}") !== false;
    }

    /**
     * @return array<string,mixed>
     */
    protected function getArgumentNamesAndDefaults()
    {
        if ($this->isTargetControlStructure())
            return self::$controlStructures[$this->targetFunctionName];
        else if (\function_exists($this->targetFunctionName))
            return parent::getArgumentNamesAndDefaults();

        // else Closure
        return array();
    }
}
