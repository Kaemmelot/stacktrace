<?php

namespace Kaemmelot\StackTrace\CallFrames;

use Kaemmelot\StackTrace\ClosureHandle;
use Kaemmelot\StackTrace\ClosureScope;
use Kaemmelot\StackTrace\Source;

class ClosureCallFrame extends FunctionCallFrame
{
    /**
     * @var ClosureScope
     */
    private $targetScope;

    /**
     * @var ClosureHandle|null
     */
    private $targetHandle;

    /**
     * @param ClosureScope       $scope
     * @param array              $args
     * @param int                $line
     * @param Source|null        $source
     * @param ClosureHandle|null $handle
     */
    public function __construct(ClosureScope $scope, array $args, $line, Source $source = null,
        ClosureHandle $handle = null)
    {
        parent::__construct("{closure}", $args, $line, $source);
        $this->targetScope = $scope;
        $this->targetHandle = $handle;
    }

    /**
     * @return ClosureScope
     */
    public function getTargetClosureScope()
    {
        return $this->targetScope;
    }

    /**
     * @return bool
     */
    public function hasTargetClosureHandle()
    {
        return $this->targetHandle !== null;
    }

    /**
     * @return ClosureHandle|null
     */
    public function getTargetClosureHandle()
    {
        return $this->targetHandle;
    }
}
