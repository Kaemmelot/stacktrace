<?php

namespace Kaemmelot\StackTrace;

use Exception;

class ClosureScope
{
    /**
     * @var string
     */
    private $namespace;

    /**
     * @var bool|null
     */
    private $static;

    /**
     * @var string
     */
    private $class;

    /**
     * @var object
     */
    private $object;

    /**
     * @param string      $closure Function name (namespace\{closure})
     * @param string|null $class
     * @param object|null $object
     * @throws Exception
     */
    public function __construct($closure, $class = null, $object = null)
    {
        if (\strpos($closure, "{closure}") === false)
            throw new Exception("Function '" . $closure . "' is not a closure");
        // namespace\{closure}
        $this->namespace = (\strlen($closure) > 9) ?
            \substr($closure, 0, \strlen($closure) - 10) : "";
        $this->class = $class;
        $this->object = $object;
        if (($class !== null) && ($object !== null))
            $this->static = false;
        else if (($class !== null) && ($object === null))
            $this->static = true;
        else
            $this->static = null;
    }

    /**
     * @return bool
     */
    public function isBound()
    {
        return $this->static !== null;
    }

    /**
     * @return string
     */
    public function getNamespace()
    {
        return $this->namespace;
    }

    /**
     * @return bool|null
     */
    public function isStatic()
    {
        return $this->static;
    }

    /**
     * @return string|null
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @return object|null
     */
    public function getObject()
    {
        return $this->object;
    }
}
